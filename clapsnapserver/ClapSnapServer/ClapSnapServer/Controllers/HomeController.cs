﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClapSnapServer.Repositories;

namespace ClapSnapServer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string filename = DateTime.UtcNow.AddHours(2).ToString("yyyy-MM-dd_HH-mm-ss") + ".jpeg";
                new FileRepository().UploadFile(filename,file.InputStream);
            }
            return RedirectToAction("Index");
        }

        public JsonResult LatestImages()
        {
            var res = new FileRepository().ListItems().OrderByDescending(x => x).Take(8).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload()
        {
            return View();
        }
    }
}
