﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ClapSnapServer.Repositories
{
    public class FileRepository
    {
        public List<string> ListItems()
        {
            return GetBlobContainer().ListBlobs().Select(blob => blob.Uri.ToString()).ToList();
        }

        public void UploadFile(string fileName, Stream fileStream)
        {
            var container = GetBlobContainer();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            blockBlob.UploadFromStream(fileStream);
        }

        public FileRepository()
        {
            var container = GetBlobContainer();
            container.CreateIfNotExists();
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

        }

        private static CloudBlobContainer GetBlobContainer()
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["BlobStorage"]);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference("mycontainer");
        }
    }
}