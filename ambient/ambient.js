var tessel = require('tessel');
var lib = require('ambient-attx4');
var ambient = lib.use(tessel.port['C']);

ambient.on('ready', function () {
console.log('ready');
  ambient.setSoundTrigger(0.1);
  ambient.on('sound-trigger', function(data) {
    console.log("Something happened with sound: ", data);
    ambient.clearSoundTrigger();
    setTimeout(function () { 
        ambient.setSoundTrigger(0.1);
    },3000);
  });
});

ambient.on('error', function (err) {
  console.log('something went wrong');
  console.log(err)
});