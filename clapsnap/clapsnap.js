var tessel = require('tessel');
var request = require('request');
var camera = require('camera-vc0706').use(tessel.port['B']);
var ambient = require('ambient-attx4').use(tessel.port['C']);
var relay = require('relay-mono').use(tessel.port['D']);
var buttonLib = require('tessel-gpio-button');
var button = tessel.port['GPIO'].pin['G3'];
var buttonEnableAudio = tessel.port['GPIO'].pin['G2'];
var wifi = require('wifi-cc3000');
var sendingFileLED = tessel.led[2];
var cameraLED = tessel.led[3]; // Set up an LED to notify when we're taking a picture
var cameraReady = false;
var relayReady = false;
var network = 'R90 ice.net'; // put in your network name here
var pass = 'CRUNA123'; // put in your password here, or leave blank for unsecured
var security = 'wpa2'; // other options are 'wep', 'wpa', or 'unsecured'
var timeouts = 0;
var takingPicture = 0;
var url = 'http://clapsnap.azurewebsites.net';	
var soundLevel = 0.2;
var lastTrigger = 0;

function connect(){
  wifi.connect({
    security: security
    , ssid: network
    , password: pass
    , timeout: 30 // in seconds
  });
}

wifi.on('connect', function(data){
  console.log("Connected to Wifi", data);
});

wifi.on('disconnect', function(data){
	console.log("Disconnected from Wifi, will try to reconnect", data);
	connect();
})

wifi.on('timeout', function(err){
	timeouts++;
	if (timeouts > 2) {
		console.log("Tried to reconnect several times, resetting chip");
		powerCycle();
	} else {
		console.log("Couldn't connect, retrying");
		connect();
	}
});

wifi.on('error', function(err){
	console.log("Wifi error: ", err);
	console.log("one of the following happened");
	console.log("1. tried to disconnect while not connected");
	console.log("2. tried to disconnect while in the middle of trying to connect");
	console.log("3. tried to initialize a connection without first waiting for a timeout or a disconnect");
});

function powerCycle(){
  // when the wifi chip resets, it will automatically try to reconnect to the last saved network
  wifi.reset(function(){
    timeouts = 0; // reset timeouts
    console.log("Done power cycling");
    // give it some time to auto reconnect
    setTimeout(function(){
		if (!wifi.isConnected()) {
			connect();
		}
    }, 20 * 1000);
  })
}

function turnOnRelay(){
	relay.turnOn(1, function turnOnResult(err) {
		if (err) {
			console.log("Err turning on relay", err);
		}else{
			console.log("Turned on relay");
		}
	});
}

function turnOffRelay(){
	relay.turnOff(1, function turnOffResult(err) {
		if (err) {
			console.log("Err turning off relay", err);
		}else{
			console.log("Turned off relay");
		}
	});
}

function sendFile(image){
	console.log('Sending image to ' + url);
	sendingFileLED.high();
	var r = request.post(url, function optionalCallback (err, httpResponse, body) {
		readyForPictures();
		if (err) {
			return console.error('upload failed:', err);
		}
		console.log('Upload successful!  Server responded with:', body);
	});
	var form = r.form();
	form.append('file', image,{
		filename: 'image.jpeg',
		contentType: 'image/jpeg'
	});
	sendingFileLED.low();	
}

function readyForPictures(){
	lastTrigger = 0;
	setTimeout(readState, 100);
}

function takePicture (){
	if (!cameraReady){
		console.log('The camera isn\'t ready');
		readyForPictures();
		return;
	}
	if (!wifi.isConnected()){
		console.log('The wifi isn\'t connected');
		connect();
		readyForPictures();
		return;
	}
	turnOnRelay();
    cameraLED.high();
	camera.takePicture(function(err, image) {
		turnOffRelay();
		cameraLED.low();
		if (err) 
		{
			console.log('Error taking image', err);
			readyForPictures();
		} 
		else 
		{
			sendFile(image);
		}
	});
}

function readState () {
	console.log("Sound Level: ", lastTrigger.toFixed(8) + ", trigger level: " + soundLevel);
	if ((lastTrigger > soundLevel && buttonEnableAudio.read() == 0) || button.read()==0){
		pictureMutex();
	}else{
		readyForPictures();
	}
}

ambient.on('ready', function () {
	console.log('The ambient module is ready');
	ambient.setSoundTrigger(soundLevel);
	ambient.on('sound-trigger', function(data) {
		
		lastTrigger = data;
	});
  
	ambient.on('error', function (err) {
		console.log('Something went wrong with the ambient module: ' + err);
		console.log(err)
	});
	readyForPictures();
});

	
camera.on('ready', function() {
	cameraReady = true;
	console.log("The camera is ready");
	camera.on('error', function(err) {
		console.error("Something went wrong with the camera: " + err);
	});
});

relay.on('ready', function relayReady () {
	relayReady = true;
	console.log('The relay is ready');
});

function pictureMutex(){
	if (!takingPicture){
		takingPicture = 1;
		takePicture();
		takingPicture = 0;
	}
}
